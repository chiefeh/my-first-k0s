#!/bin/bash

if [ ! -f /usr/bin/k0s ]; then
	curl -sSLf https://get.k0s.sh | sudo sh
	echo "installing k0s"
	sudo k0s install controller --enable-worker
	sudo systemctl start k0scontroller
	sudo systemctl enable k0scontroller
	sleep 10
else
	echo "k0s already installed"
	sudo k0s install controller --enable-worker
	sudo systemctl start k0scontroller
	sudo systemctl enable k0scontroller
	sleep 10
fi


# Print Status
sudo k0s status

if [ ! -f ~/.kube/config ]; then
        echo "File not found!"
	mkdir ~/.kube
else
        rm ~/.kube/config
        echo "deleted old kube config"
fi

sudo cp /var/lib/k0s/pki/admin.conf ~/.kube/config
echo "copy admin.conf to kube/config"
sudo chown gregory:gregory ~/.kube/config
echo "chown kube/config"
sudo chmod 700 ~/.kube/config
echo "chmod kube/config"
watch kubectl get all -A
